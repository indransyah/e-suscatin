<?php

View::composer(['frontend.layouts.header', 'frontend.layouts.footer'], function($view)
{
  $institute = Institute::find(1);
  $web = Web::find(1);
  $pages = Page::where('slug', '!=', 'beranda')->get();
  $view->with(array('institute' => $institute, 'web' => $web, 'pages' => $pages));
});

View::composer(['frontend.layouts.sidebar'], function($view)
{
  if(Sentry::check() && Sentry::getUser()->hasAccess('pengantin'))
  {
    $testimony = Testimony::where('user_id', Sentry::getUser()->id)->first();
    $view->with(array('testimony' => $testimony));
  }
});

View::composer(['backend.login', 'backend.layouts.header'], function($view)
{
  $web = Web::find(1);
  $view->with('web', $web);
});

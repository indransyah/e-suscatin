<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return View::make('hello');
// });



// Route::get('masuk', 'BerandaController@getMasuk');
Route::post('masuk', 'BerandaController@postMasuk');
Route::get('keluar', 'BerandaController@getKeluar');
Route::get('/', 'BerandaController@getIndex');
Route::controller('halaman', 'HalamanController');

Route::group(['before' => 'pengantin'], function() {
	Route::controller('materi', 'MateriController');
	Route::controller('pengantin', 'PengantinController');
	// Route::controller('halaman', 'HalamanController');
	Route::controller('testimoni', 'TestimoniController');
});

Route::group(['prefix' => 'admin'], function() {
	Route::get('masuk', 'AdminBerandaController@getMasuk');
	Route::post('masuk', 'AdminBerandaController@postMasuk');

	Route::group(['before' => 'admin'], function(){
		Route::get('/', 'AdminBerandaController@getIndex');
		Route::controller('materi', 'AdminMateriController');
		Route::controller('petugas', 'AdminPetugasController');
		Route::controller('pengantin', 'AdminPengantinController');
		Route::controller('halaman', 'AdminHalamanController');
		Route::controller('instansi', 'AdminInstansiController');
		Route::controller('testimoni', 'AdminTestimoniController');
		Route::get('keluar', 'AdminBerandaController@getKeluar');
	});
});

Event::subscribe('PengantinSubscriber');

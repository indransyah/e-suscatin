<?php

class Page extends Eloquent {

	protected $table = 'pages';

	protected $guarded = array('id');

	public static $rules = array(
		'judul' => 'required|unique:pages,title',
		'isi' =>'required'
	);
	
}
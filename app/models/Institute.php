<?php

class Institute extends Eloquent {

	protected $table = 'institute';

	protected $guarded = array('id');

	public static $rules = array(
        'nama' => 'required',
        'telp' =>'required|digits_between:9,12',
        'alamat' =>'required',
    );

}

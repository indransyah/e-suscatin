<?php

class Person extends Eloquent {

	protected $table = 'person';

	protected $guarded = array('id');

	public function couple()
    {
        return $this->belongsTo('Couple');
    }
	
}
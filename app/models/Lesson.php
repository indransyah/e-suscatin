<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Lesson extends Eloquent implements StaplerableInterface {
    use EloquentTrait;

	protected $table = 'lessons';

	protected $guarded = array('id');

	public static $rules = array(
		'judul' => 'required',
		'file' =>'mimes:doc,docx,pdf,ppt,pptx'
	);

	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('file');
		parent::__construct($attributes);
	}

}

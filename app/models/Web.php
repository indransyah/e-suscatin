<?php

class Web extends Eloquent {

	protected $table = 'web';

	protected $guarded = array('id');

	public static $rules = array(
        'nama' => 'required',
        'description' =>'required',
    );

}

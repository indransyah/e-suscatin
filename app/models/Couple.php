<?php

class Couple extends Eloquent {

	protected $table = 'couple';

	protected $guarded = array('id');

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function person()
	{
		return $this->hasMany('Person');
	}

}

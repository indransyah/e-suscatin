<?php

class Testimony extends Eloquent {

	protected $table = 'testimonies';

	protected $guarded = array('id');

	public static $rules = array(
		'testimoni' => 'required',
	);

	public function user()
	{
		return $this->belongsTo('User');
	}

}

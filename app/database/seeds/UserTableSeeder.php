<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();
		
		// Create Admin
		$user = Sentry::createUser(array(
			'name'		=> 'Administrator',
			'username'     => 'admin',
			'password'  => 'admin',
			'activated' => true,
			));

    	// Find the group using the group id
		$administratorGroup = Sentry::findGroupByName('Administrator');

    	// Assign the group to the user
		$user->addGroup($administratorGroup);

		// Create Pengantin
		// $user = Sentry::createUser(array(
		// 	'name'		=> 'Indransyah',
		// 	'email'     => 'indransyah@gmail.com',
		// 	'password'  => 'customer',
		// 	'activated' => true,
		// 	));

    	// Find the group using the group id
		// $pengantinGroup = Sentry::findGroupByName('Pengantin');

    	// Assign the group to the user
		// $user->addGroup($pengantinGroup);

	}

}

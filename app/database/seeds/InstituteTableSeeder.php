<?php

class InstituteTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('institute')->delete();

		$institute = Institute::create(array(
			'name'		=> 'Kantor Urusan Agama Kec. Manyaran Kab. Wonogiri',
			'phone'     => '0273531324',
			'address'  => 'Jl Karanglor-Manyaran',
			));

	}

}

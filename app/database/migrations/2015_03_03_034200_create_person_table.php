<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->enum('religion', ['islam', 'kristen', 'katolik', 'hindu', 'budha', 'konghucu']);
			$table->string('phone', 12);
			$table->string('birth');
			$table->text('address');
			$table->enum('status', ['jejaka','duda cerai', 'duda mati', 'perawan', 'janda cerai', 'janda mati']);
			$table->enum('gender', ['male', 'female']);
			$table->integer('couple_id')->unsigned();
			$table->foreign('couple_id')->references('id')->on('couple')->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person');
	}

}

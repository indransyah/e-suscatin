<?php

class Helpers {
	
	public static function date($date) {
		return date("d F Y",strtotime($date)).' @ '.date("G:i",strtotime($date));
	}

}
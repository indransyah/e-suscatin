<?php

/**
*
*/
class PengantinSubscriber
{

	public function onCreateUser ($input) {
		 $user = Sentry::createUser(array(
        	'name'      => $input['nama-pengantin-pria'],
        	'username'  => $input['telp-pengantin-pria'],
        	'email'     => $input['telp-pengantin-pria'],
        	'password'  => 'kua manyaran',
        	'activated' => true,
        ));
        // Find the group using the group id
        $pengantinGroup = Sentry::findGroupByName('Pengantin');
        // Assign the group to the user
        $user->addGroup($pengantinGroup);
		Event::fire('create.couple', array($input, $user->id));

	}

	public function onCreateCouple ($input, $user_id) {
		$couple = new Couple;
		$couple->user_id = $user_id;;
		$couple->save();
		// return $couple->id;

		$gender = array('male', 'female');
		foreach ($gender as $value) {
			Event::fire('create.person', array($input, $value, $couple->id));
		}
	}

	public function onCreatePerson($input, $gender, $couple_id) {
		switch ($gender) {
			case 'male':
				$prefix = '-pengantin-pria';
				break;

			case 'female':
				$prefix = '-pengantin-wanita';
				break;
		}

		$person = new Person;
		$person->gender = $gender;
		$person->name = $input['nama'.$prefix];
		$person->phone = $input['telp'.$prefix];
		$person->religion = $input['agama'.$prefix];
		$person->birth = $input['ttl'.$prefix];
		$person->address = $input['alamat'.$prefix];
		$person->status = $input['status'.$prefix];
		$person->couple_id = $couple_id;
		$person->save();
	}

	public function onUpdatePerson($couple_id, $input) {
		$prefix = array(
			'-pengantin-pria',
			'-pengantin-wanita'
		);
		$couple = Couple::find($couple_id);
		/* Male */
		$couple->person[0]->name = $input['nama'.$prefix[0]];
		$couple->person[0]->phone = $input['telp'.$prefix[0]];
		$couple->person[0]->religion = $input['agama'.$prefix[0]];
		$couple->person[0]->birth = $input['ttl'.$prefix[0]];
		$couple->person[0]->address = $input['alamat'.$prefix[0]];
		$couple->person[0]->status = $input['status'.$prefix[0]];
		$couple->person[0]->save();
		/* Female */
		$couple->person[1]->name = $input['nama'.$prefix[1]];
		$couple->person[1]->phone = $input['telp'.$prefix[1]];
		$couple->person[1]->religion = $input['agama'.$prefix[1]];
		$couple->person[1]->birth = $input['ttl'.$prefix[1]];
		$couple->person[1]->address = $input['alamat'.$prefix[1]];
		$couple->person[1]->status = $input['status'.$prefix[1]];
		$couple->person[1]->save();

	}

	public function subscribe($events) {
		$events->listen('create.user', 'PengantinSubscriber@onCreateUser');
		$events->listen('create.couple', 'PengantinSubscriber@onCreateCouple');
		$events->listen('create.person', 'PengantinSubscriber@onCreatePerson');
		$events->listen('update.person', 'PengantinSubscriber@onUpdatePerson');
	}
}

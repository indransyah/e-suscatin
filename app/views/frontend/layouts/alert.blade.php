@if (Session::has('danger'))
<div class="alert alert-danger square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('danger') }}
    {{ HTML::ul($errors->all()) }}
</div>
@endif

@if (Session::has('info'))
<div class="alert alert-info square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('info') }}
</div>
@endif

@if (Session::has('warning'))
<div class="alert alert-warning square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('warning') }}
</div>
@endif

@if (Session::has('success'))
<div class="alert alert-success square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('success') }}
</div>
@endif
    <!--
    <div class="alert alert-success square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Well done!</strong> You successfully read this
    <a href="#" class="alert-link">important alert message.</a>
    </div>
     
    <div class="alert alert-info square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Heads up!</strong> This alert needs your attention, but
    <a href="#" class="alert-link">its not super important.</a>
    </div>
     
    <div class="alert alert-warning square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Warning!</strong> Better check yourself, youre
    <a href="#" class="alert-link">not looking too good.</a>
    </div>
     
    <div class="alert alert-danger square fade in alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Oh snap!</strong> Change a few things up and
    <a href="#" class="alert-link">try submitting again.</a>
    </div>
    -->
		<div class="header">
			<div class="container">
				<div class="top-header">
					<div class="container-fluid bg-dark-success">
						<div class="col-md-2 col-xs-12">
							<img class="logo img-responsive img-center" src="{{ URL::to('assets/frontend/logo.png') }}">
						</div>
						<div class="col-md-10 col-xs-12 text-center">
							<h2 class="title" style="margin-top:15px;">{{ $web->name }} ({{ $web->description }})</h2>
							<h2 class="title">{{ $institute->name }}</h2>
							<h4 class="title" style="margin-bottom:10px;">{{ $institute->address }} Telp ({{ $institute->phone }})</h4>

						</div>
					</div>
					<nav class="navbar square navbar-success" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<!-- <a class="navbar-brand" href="#fakelink">Brand</a> -->
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav">
									<li class="{{ Active::action('BerandaController@getIndex') }}"><a href="{{ URL::action('BerandaController@getIndex') }}"><i class="fa fa-home"></i> Beranda</a></li>
									<li class="{{ Active::action(['MateriController@getIndex', 'MateriController@getLihat']) }}"><a href="{{ URL::action('MateriController@getIndex') }}"><!-- <i class="fa fa-file"></i> --> Materi</a></li>
									@foreach($pages as $page)
									<li class="{{ Active::pattern('halaman/lihat/'.$page->slug) }}"><a href="{{ URL::action('HalamanController@getLihat', $page->slug) }}">{{ $page->title }}</a></li>
									@endforeach
									<!-- <li><a href="#fakelink"><i class="fa fa-info"></i> Panduan</a></li>
									<li><a href="#fakelink"><i class="fa fa-question"></i> Tentang</a></li>
									<li class="dropdown">
										<a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
										<ul class="dropdown-menu square margin-list-rounded with-triangle">
											...
										</ul>
									</li> -->
								</ul>
								{{ Form::open(array('action' => 'MateriController@getCari', 'method' => 'get', 'class' => 'navbar-form navbar-left', 'role' => 'search')) }}
									<div class="form-group">
										{{ Form::text('q', null, array('class' => 'form-control', 'placeholder' => 'Cari materi', 'required' => 'true' )) }}
									</div>
									<button type="submit" class="btn btn-success btn-square">Cari</button>
								</form>
								@if(Sentry::check() && Sentry::getUser()->hasAccess('pengantin'))
								<ul class="nav navbar-nav navbar-right">
									<li class="dropdown">
										<a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->name }} <b class="caret"></b></a>
										<ul class="dropdown-menu success square margin-list-rounded with-triangle">
											<li><a href="{{ URL::action('PengantinController@getPassword') }}"><i class="fa fa-key"></i> Password</a></li>
											<li><a href="{{ URL::action('BerandaController@getKeluar') }}"><i class="fa fa-power-off"></i> Keluar</a></li>
										</ul>
									</li>
								</ul>
								@endif
							</div><!-- /.navbar-collapse -->
						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div><!-- /.container -->
		</div><!-- /.header -->

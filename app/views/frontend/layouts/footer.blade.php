		<footer>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h4>Halaman</h4>
						<ul>
							@foreach($pages as $page)
							<li><a href="{{ URL::action('HalamanController@getLihat', $page->slug) }}">{{ $page->title }}</a></li>
							@endforeach
							<!-- <li><a href="#fakelink">Company</a></li>
							<li><a href="#fakelink">Press</a></li>
							<li><a href="#fakelink">Branch office</a></li>
							<li><a href="#fakelink">Timeline history</a></li> -->
						</ul>
					</div>
					<div class="col-sm-6">
						<h4>Hubungi Kami</h4>
						<ul>
							<li>{{ $institute->name }}</li>
							<li>{{ $institute->address }} Telp {{ $institute->phone }}</li>
						</ul>
					</div>
					<!-- <div class="col-sm-3">
						<h4>PAGES</h4>
						<ul>
							<li><a href="#fakelink">Terms and condition</a></li>
							<li><a href="#fakelink">Privacy policy</a></li>
							<li><a href="#fakelink">FAQ</a></li>
							<li><a href="#fakelink">Advertise</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h4>SUBSCRIBE</h4>
						<form role="form">
							<input type="email" class="form-control" placeholder="Enter email...">
						</form>
						<h4>FOLLOW US</h4>
						<p>
							<a href="#fakelink"><i class="fa fa-facebook icon-xs icon-primary icon-circle"></i></a>
							<a href="#fakelink"><i class="fa fa-twitter icon-xs icon-primary icon-circle"></i></a>
							<a href="#fakelink"><i class="fa fa-youtube icon-xs icon-primary icon-circle"></i></a>
							<a href="#fakelink"><i class="fa fa-pinterest icon-xs icon-primary icon-circle"></i></a>
							<a href="#fakelink"><i class="fa fa-rss icon-xs icon-primary icon-circle"></i></a>
						</p>
					</div> -->
				</div>
			</div>

			<!--
			<div class="footer">
			&copy; 2014 Your Website
			</div>
			-->

		</footer>

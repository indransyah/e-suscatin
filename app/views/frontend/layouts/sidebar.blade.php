					<div class="col-sm-4">
						@if(Sentry::check() && Sentry::getUser()->hasAccess('pengantin'))
						<div class="panel panel-square panel-success">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-user"></i> Profil Pengantin</h3>
							</div>
							<div class="panel-body">
								@foreach(User::person(Sentry::getUser()->id) as $person)
									@if($person->gender == 'male')
										<p><strong>Pengantin Pria</strong></p>
									@elseif($person->gender == 'female')
										<br><p><strong>Pengantin Wanita</strong></p>
									@endif
									Nama : {{ $person->name }}<br>
									TTL : {{ $person->birth }}<br>
									No Telp : {{ $person->phone }}<br>
									Agama : {{ ucwords($person->religion) }}<br>
									Status : {{ ucwords($person->status) }}<br>
									Alamat : {{ ($person->address == null) ? '-' : $person->address }}<br>
								@endforeach
							</div>
						</div>
						<div class="panel panel-square panel-success">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-comment-o"></i> Testimoni</h3>
							</div>
							<div class="panel-body">
								@if(is_null($testimony))
								{{ Form::open(array('action' => 'TestimoniController@postKirim')) }}
								<input name="redirect" type="hidden" value="{{ Request::path() }}">
								<div class="form-group">
									<label>Tulis komentar Anda?</label>
									{{ Form::textarea('testimoni', null, array('class' => 'form-control', 'rows' => '8', 'placeholder' => 'Tuliskan pendapat Anda', 'required' => 'true' )) }}
									<br>
									<button class="btn btn-success btn-square pull-right">Kirim <i class="fa fa-send"></i></button>
								</div>
								@else
								<p><strong>Pendapat Anda</strong></p>
								<p><i>"{{ $testimony->testimony }}"</i></p>
								@endif

							</div>
						</div>
						@else
						<div class="panel panel-square panel-success">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-key"></i> Login</h3>
							</div>
							<div class="panel-body">
								{{ Form::open(array('action' => 'BerandaController@postMasuk')) }}
								<div class="form-group">
									<label>Username</label>
									{{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'No telp pengantin pria', 'required' => 'true' )) }}
									<br>
									<label>Password</label>
									{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true' )) }}
									<br>
									<p class="help-block"><i>Gunakan password "kua manyaran" (tanpa tanda kutip) jika Anda belum mengubah passwordnya!</i></p>
									<button class="btn btn-success btn-square pull-right">Masuk <i class="fa fa-angle-double-right"></i></button>
								</div>
								{{ Form::close() }}
							</div>
						</div>

						@endif
						<!--
						<h4 class="sidebar-title">LOGIN</h4>
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" placeholder="No telp, misal : 085729968839">
							<br>
							<label>Password</label>
							<input type="text" class="form-control" placeholder="Password">
							<br>
							<p class="help-block"><i>Gunakan no telp pengantin pria sebagai username dengan password "kua manyaran" (tanpa tanda kutip) jika Anda pertama kali login ke dalam e-learning</i></p>
							<button class="btn btn-success btn-square pull-right">Masuk</button>
						</div>
						-->
					</div><!-- /.col-sm-3 -->

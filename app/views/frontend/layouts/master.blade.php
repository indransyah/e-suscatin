<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
		<meta name="description" content="{{ Web::find(1)->name }} - {{ Web::find(1)->description }}">
		<meta name="keywords" content="{{ Web::find(1)->description }}">
		<meta name="author" content="{{ Institute::find(1)->name }}">
		<title>{{ Web::find(1)->name }} - {{ Web::find(1)->description }} | {{ Institute::find(1)->name }}</title>

		<!-- MAIN CSS -->
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/css/bootstrap.min.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/css/ndoboost.min.css') }}

		<!-- PLUGINS CSS -->
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/font-awesome/css/font-awesome.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/weather-icon/css/weather-icons.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/prettify/prettify.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/magnific-popup/magnific-popup.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/plugins/owl-carousel/owl.carousel.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/owl-carousel/owl.theme.css') }}
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/plugins/owl-carousel/owl.transitions.css') }}

		<!-- MAIN CSS -->
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
		{{ HTML::style('assets/frontend/ndoboost-1.0.0/dist/css/style.css') }}

		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- My Style -->
		<style type="text/css">
		.container {
			max-width: 1000px;
		}
		.logo {
			padding: 10px;
		}
		.title {
			font-family: 'Times New Roman', sans-serif;
			color: #FFF;
			font-weight: bold;
			margin-top: 12px;
			margin-bottom: 0;
		}
		a {
			color: #00A65A;
		}
		a:hover {
			color: #00A65A;

		}
		.bg-dark-success {
			background-color: #008749;
		}
		.text-dark-success {
			color: #7EB446;
		}
		.panel-square {
			/*border-radius: 0px;*/
		}
		.panel-success {
			/*border-color: #d6e9c6;*/
		}
		.panel-heading {
			padding: 10px 15px;
			border-bottom: 1px solid transparent;
			/*border-radius: 0px;*/

		}
		.panel-success > .panel-heading {
			/*color: #3c763d;*/
			color: #FFF;
			background-color: #00A65A;
			/*border-color: #d6e9c6;*/
		}
		.panel-success > .panel-heading + .panel-collapse .panel-body {
			border-top-color: #d6e9c6;
		}
		.panel-success > .panel-footer + .panel-collapse .panel-body {
			border-bottom-color: #d6e9c6;
		}
		.navbar-success {
			background-color: #00A65A;
			border-color: #008749;
		}
		.navbar-success .btn.btn-success,
		.navbar-success .btn.btn-success:hover,
		.navbar-success .btn.btn-success:focus,
		.navbar-success .btn.btn-success:active {
			background-color: #008749;
			border-color: #008749;
		}
		.navbar-success .navbar-nav > .active > a,
		.navbar-success .navbar-nav > .active > a:hover,
		.navbar-success .navbar-nav > .active > a:focus {
			color: #fff;
			background-color: #008749;
		}
		.navbar-success .navbar-text 		{color: #fff;}
		.navbar-success .navbar-nav > li > a 	{color: #fff;}
		.navbar-success .navbar-nav > li > a:hover,
		.navbar-success .navbar-nav > li > a:focus {
			color: #F2FFE5;
			/*background-color: transparent;*/
			background-color: #008749;
		}
		.navbar-success .navbar-toggle 		{border-color: #008749;}
		.navbar-success .navbar-toggle:hover,
		.navbar-success .navbar-toggle:focus {
			background-color: #008749;
		}
		.navbar-success .navbar-toggle .icon-bar {background-color: #fff;}
		.navbar-success .navbar-collapse,
		.navbar-success .navbar-form {
			border-color: #008749;
		}
		.navbar-success .navbar-nav > .open > a,
		.navbar-success .navbar-nav > .open > a:hover,
		.navbar-success .navbar-nav > .open > a:focus {
			color: #fff;
			background-color: #008749;
		}
		.dropdown-menu.success > li > a:hover,
		.dropdown-menu.success > li > a:focus {
			color: #fff;
			background-color: #008749;
		}
		.btn-success {
			background-color: #00A65A;
			border-color: #00A65A;
		}
		.btn-success:hover,
		.btn-success:focus,
		.btn-success:active,
		.btn-success.active,
		.open .dropdown-toggle.btn-success {
			background-color: #008749;
			border-color: #008749;
		}
		.img-center {
			margin: 0 auto;
		}
		.bg-purple {
    	background-color: #605CA8 !important;
			opacity: 0.65;
		}
		.bg-green {
    	background-color: #00A65A !important;
			opacity: 0.65;
		}
		.esuscatin {
			color: #605CA8 !important;
			opacity: 0.65;
			font-size: 45px;
			margin-right: 5px;
		}
		</style>

	</head>

	<body id="top" class="tooltips">

		<!--
		===========================================================
		BEGIN PAGE
		===========================================================
		-->

		@include('frontend.layouts.header')

		<div class="page-content" style="padding:0;">
			<div class="container">
				@include('frontend.layouts.alert')
				<div class="row">
        			@yield('content')

					@include('frontend.layouts.sidebar')

				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.page-content -->

		@include('frontend.layouts.footer')

		<!--
		===========================================================
		END PAGE
		===========================================================
		-->

		<!--
		===========================================================
		Placed at the end of the document so the pages load faster
		===========================================================
		-->
		<!-- MAIN JAVASRCIPT  -->
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/js/jquery.js') }}
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/js/bootstrap.min.js') }}

		<!-- PLUGINS AND NDOBOOST JS-->
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/plugins/prettify/prettify.js') }}
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/plugins/magnific-popup/jquery.magnific-popup.min.js') }}
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/plugins/owl-carousel/owl.carousel.js') }}
		{{ HTML::script('assets/frontend/ndoboost-1.0.0/dist/js/ndoboost.min.js') }}

	</body>
</html>

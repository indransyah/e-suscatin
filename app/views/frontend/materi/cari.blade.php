@extends('frontend.layouts.master')
@section('content')
					<div class="col-sm-8">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">Materi dengan kata kunci "{{ $keyword }}"</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						@if($lesson->count() == 0)
						<p>Tidak ada materi</p>
						@else
						<div class="list-group">
							@foreach($lesson as $value)
							<a href="{{ URL::action('MateriController@getLihat', $value->id) }}" class="list-group-item">
								{{ $value->title }}
							</a>
							@endforeach
						</div>
						@endif
					</div><!-- /.col-sm-9 -->
@stop

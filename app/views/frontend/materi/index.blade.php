@extends('frontend.layouts.master')
@section('content')
<style>
.bg-yellow {
	background-color: yellow;
}
.bg-green {
	background-color: green;
}
a.list-group-item {
	color: #000;
}
</style>
					<div class="col-sm-8">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">Daftar Materi</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						@if($lesson->count() == 0)
						<p>Tidak ada materi</p>
						@else
						<div class="list-group">
							<?php $i = 1; ?>
							@foreach($lesson as $value)
							<a href="{{ URL::action('MateriController@getLihat', $value->id) }}" class="list-group-item {{ ($i%2 == 0) ? 'bg-yellow' : 'bg-green' }}">
								<strong>{{ $value->title }}</strong>
							</a>
							<?php $i++; ?>
							@endforeach
						</div>
						@endif
					</div><!-- /.col-sm-9 -->
@stop

@extends('frontend.layouts.master')
@section('content')
					<div class="col-sm-8">
						<div class="well {{ ($lesson->id%2 == 0) ? 'bg-purple' : 'bg-green' }}" style="color:#FFF;">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">{{ $lesson->title }}</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						<p>{{ $lesson->description }}</p>
						<!-- <iframe src="http://docs.google.com/viewer?url=linuxgeekers.com/uploads/kua/2. Konsep perkwinan dalam Islam.doc&embedded=true" width="600" height="780" style="border: none;"></iframe> -->
						@if(!is_null($lesson->file_file_name))
						<iframe src="http://docs.google.com/viewer?url={{ URL::to($lesson->file->url()) }}&embedded=true" height="600" style="border: none;"></iframe>
						<!-- <iframe src="http://docs.google.com/viewer?url=linuxgeekers.com/uploads/kua/2. Konsep perkwinan dalam Islam.doc&embedded=true" height="600" style="border: none;"></iframe> -->
						@endif
						<!-- <iframe src="http://view.officeapps.live.com/op/view.aspx?src=newteach.pbworks.com%2Ff%2Fele%2Bnewsletter.docx" height="600" style="border: none;"></iframe> -->
						</div>
					</div><!-- /.col-sm-9 -->
@stop

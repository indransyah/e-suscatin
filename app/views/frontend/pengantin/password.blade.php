@extends('frontend.layouts.master')
@section('content')					
					<div class="col-sm-8">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">Sunting Password</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						<!-- <hr> -->
						<div class="form-group">
                			{{ Form::open(array('action' => 'PengantinController@postPassword')) }}
							<label>Password saat ini</label>
                      		{{ Form::password('current_password', array('class' => 'form-control', 'placeholder' => 'Password yang Anda gunakan saat ini', 'required' => 'true' )) }}
							<p class="help-block"><i>Gunakan password "kua manyaran" jika Anda belum pernah mengubah password Anda.</i></p>
							<!-- <br> -->
							<label>Password baru</label>
                      		{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password baru yang akan digunakan', 'required' => 'true' )) }}
							<br>
							<label>Konfirmasi password baru</label>
                      		{{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Konfirmasi password baru (harus sama dengan Password baru)', 'required' => 'true' )) }}
							<br>
							<!-- <p class="help-block"><i>Gunakan no telp pengantin pria sebagai username dengan password "kua manyaran" (tanpa tanda kutip) jika Anda pertama kali login ke dalam e-learning</i></p> -->
							<button class="btn btn-success btn-square pull-right">Sunting</button>
							{{ Form::close() }}
						</div>
					</div><!-- /.col-sm-9 -->
@stop
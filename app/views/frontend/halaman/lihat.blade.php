@extends('frontend.layouts.master')
@section('content')
					<div class="col-sm-8">
						<div class="well {{ ($page->slug == 'fokus-materi') ? 'bg-purple' : 'bg-green' }}" style="color:#FFF;">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">{{ $page->title }}</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						{{ $page->content }}
						</div>
						@if($page->slug == 'fokus-materi')
						<div class="well" style="background-color: #FFF;">
							<p>Manajemen konflik dalam rumah tangga</p>
							<p><iframe frameborder="0" height="600" scrolling="yes" src="http://docs.google.com/viewer?url=https://dl.dropboxusercontent.com/u/40096191/Manajemen%20Konflik%20Dalam%20Rumah%20Tangga%20Power%20point.pptx&amp;embedded=true" width="100%"></iframe></p>
						</div>
						@endif
					</div><!-- /.col-sm-9 -->
@stop

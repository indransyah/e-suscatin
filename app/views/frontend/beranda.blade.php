@extends('frontend.layouts.master')
@section('content')
					<div class="col-sm-8">
						<h1 class="sidebar-title" style="margin-top: 0px; margin-bottom:10px;">{{ $data['title'] }}</h1>
						<hr style="margin-top:5px;margin-bottom:25px;">
						<p>{{ $data['content'] }}</p>
					</div><!-- /.col-sm-9 -->
@stop

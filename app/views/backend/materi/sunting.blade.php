@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Materi
            <small>menambah materi yang terdapat pada e-learning</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Materi</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('action' => ['AdminMateriController@postSunting', $lesson->id], 'files' => 'true')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul materi *</label>
                      {{ Form::text('judul', $lesson->title, array('class' => 'form-control', 'placeholder' => 'Judul materi yang akan ditambahkan', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Deskripsi materi</label>
                      {{ Form::textarea('deskripsi', $lesson->description, array('class' => 'form-control', 'placeholder' => 'Deskripsi materi yang akan ditambahkan' )) }}
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File materi</label>
                      <p>File materi yang aktif : <a target="blank" href="{{ $lesson->file->url() }}">{{ $lesson->file_file_name }}</a></p>
                      <hr>
                      <input name ="file" type="file" id="exampleInputFile">
                      <p class="help-block"><i>Upload file materi terbaru untuk memperbarui file materi (.doc, .docx, .pdf, .ppt, .pptx).</i></p>
                      <hr>
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Sunting <i class="fa fa-edit"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop

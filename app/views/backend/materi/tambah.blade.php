@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Materi
            <small>menambah materi yang terdapat pada e-learning</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Materi</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('action' => 'AdminMateriController@postTambah', 'files' => 'true')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul materi *</label>
                      {{ Form::text('judul', null, array('class' => 'form-control', 'placeholder' => 'Judul materi yang akan ditambahkan', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Deskripsi materi</label>
                      {{ Form::textarea('deskripsi', null, array('class' => 'form-control', 'placeholder' => 'Deskripsi materi yang akan ditambahkan' )) }}
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File materi</label>
                      <input name ="file" type="file" id="exampleInputFile">
                      <p class="help-block">Pilih file materi yang akan diupload (.doc, .docx, .pdf, .ppt, .pptx).</p>
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Tambah <i class="fa fa-plus"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Masuk</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    {{ HTML::style('assets/backend/AdminLTE/bootstrap/css/bootstrap.min.css') }}
    <!-- Font Awesome Icons -->
    {{ HTML::style('assets/backend/AdminLTE/font-awesome-4.3.0/css/font-awesome.min.css') }}
    <!-- Theme style -->
    {{ HTML::style('assets/backend/AdminLTE/dist/css/AdminLTE.min.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <!-- <a href="#">e-Learning <b>KUA</b></a> -->
        <a href="#">{{ $web->name }}</a>
      </div><!-- /.login-logo -->
      @include('backend.layouts.callout')
      <div class="login-box-body">
        {{ Form::open(array('action' => 'AdminBerandaController@postMasuk')) }}
          <div class="form-group has-feedback">
            {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username', 'required' => 'true' )) }}
          </div>
          <div class="form-group has-feedback">
            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true' )) }}
          </div>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-success btn-block btn-flat">Masuk</button>
            </div><!-- /.col -->
          </div>
        {{ Form::close() }}
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.3 -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') }}
    <!-- Bootstrap 3.3.2 JS -->
    {{ HTML::script('assets/backend/AdminLTE/bootstrap/js/bootstrap.min.js') }}
  </body>
</html>

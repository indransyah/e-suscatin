      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div> -->
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <!-- <li class="header">MAIN NAVIGATION</li> -->
            <!-- <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Beranda</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul>
            </li> -->
            <li class="{{ Active::action('AdminBerandaController@getIndex') }}"><a href="{{ URL::action('AdminBerandaController@getIndex') }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="treeview {{ Active::action(['AdminHalamanController@getIndex', 'AdminHalamanController@getTambah', 'AdminHalamanController@getSunting']) }}">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Halaman</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action('AdminHalamanController@getTambah') }}"><a href="{{ URL::action('AdminHalamanController@getTambah') }}"><i class="fa fa-plus"></i> Tambah Halaman</a></li>
                <li class="{{ Active::action('AdminHalamanController@getIndex') }}"><a href="{{ URL::action('AdminHalamanController@getIndex') }}"><i class="fa fa-eye"></i> Lihat Daftar Halaman</a></li>
              </ul>
            </li>
            <li class="treeview {{ Active::action(['AdminMateriController@getIndex', 'AdminMateriController@getTambah', 'AdminMateriController@getSunting']) }}">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Materi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action('AdminMateriController@getTambah') }}"><a href="{{ URL::action('AdminMateriController@getTambah') }}"><i class="fa fa-plus"></i> Tambah Materi</a></li>
                <li class="{{ Active::action('AdminMateriController@getIndex') }}"><a href="{{ URL::action('AdminMateriController@getIndex') }}"><i class="fa fa-eye"></i> Lihat Daftar Materi</a></li>
              </ul>
            </li>
            <li class="treeview {{ Active::action(['AdminPengantinController@getIndex', 'AdminPengantinController@getTambah', 'AdminPengantinController@getDetail', 'AdminPengantinController@getSunting']) }}">
              <a href="#">
                <i class="fa fa-circle-o"></i>
                <span>Pengantin</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action('AdminPengantinController@getTambah') }}"><a href="{{ URL::action('AdminPengantinController@getTambah') }}"><i class="fa fa-plus"></i> Tambah Pengantin</a></li>
                <li class="{{ Active::action('AdminPengantinController@getIndex') }}"><a href="{{ URL::action('AdminPengantinController@getIndex') }}"><i class="fa fa-eye"></i> Lihat Daftar Pengantin</a></li>
              </ul>
            </li>
            <li class="treeview {{ Active::action(['AdminTestimoniController@getIndex']) }}">
              <a href="#">
                <i class="fa fa-comment-o"></i>
                <span>Testimoni</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action('AdminTestimoniController@getIndex') }}"><a href="{{ URL::action('AdminTestimoniController@getIndex') }}"><i class="fa fa-eye"></i> Lihat Testimoni</a></li>
              </ul>
            </li>
            <li class="treeview {{ Active::action(['AdminPetugasController@getIndex', 'AdminPetugasController@getTambah', 'AdminPetugasController@getProfil', 'AdminPetugasController@getPassword']) }}">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Petugas</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action('AdminPetugasController@getTambah') }}"><a href="{{ URL::action('AdminPetugasController@getTambah') }}"><i class="fa fa-plus"></i> Tambah Petugas</a></li>
                <li class="{{ Active::action('AdminPetugasController@getIndex') }}"><a href="{{ URL::action('AdminPetugasController@getIndex') }}"><i class="fa fa-eye"></i> Lihat Daftar Petugas</a></li>
                <li class="{{ Active::action(['AdminPetugasController@getProfil', 'AdminPetugasController@getPassword']) }}"><a href="{{ URL::action('AdminPetugasController@getProfil') }}"><i class="fa fa-user"></i> Profil Petugas</a></li>
              </ul>
            </li>
            <li class="treeview {{ Active::action(['AdminInstansiController@getIndex', 'AdminInstansiController@getProfil']) }}">
              <a href="#">
                <i class="fa fa-sitemap"></i>
                <span>Instansi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Active::action(['AdminInstansiController@getProfil', 'AdminPetugasController@getPassword']) }}"><a href="{{ URL::action('AdminInstansiController@getProfil') }}"><i class="fa fa-sitemap"></i> Profil Instansi</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    {{ HTML::style('assets/backend/AdminLTE/bootstrap/css/bootstrap.min.css') }}
    <!-- FontAwesome 4.3.0 -->
    {{ HTML::style('assets/backend/AdminLTE/font-awesome-4.3.0/css/font-awesome.min.css') }}
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    {{ HTML::style('assets/backend/AdminLTE/dist/css/AdminLTE.min.css') }}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {{ HTML::style('assets/backend/AdminLTE/dist/css/skins/_all-skins.min.css') }}
    <!-- iCheck -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/iCheck/flat/blue.css') }}
    <!-- Morris chart -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/morris/morris.css') }}
    <!-- jvectormap -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}
    <!-- Date Picker -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/datepicker/datepicker3.css') }}
    <!-- Daterange picker -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css') }}
    <!-- bootstrap wysihtml5 - text editor -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}

    <!-- DATA TABLES -->
    {{ HTML::style('assets/backend/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-green">
    <div class="wrapper">
      @include('backend.layouts.header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('backend.layouts.sidebar')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        @yield('content')
      </div><!-- /.content-wrapper -->
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') }}
    <!-- jQuery UI 1.11.2 -->
    {{ HTML::script('assets/backend/AdminLTE/jquery-ui-1.11.2/jquery-ui.min.js') }}
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    {{ HTML::script('assets/backend/AdminLTE/bootstrap/js/bootstrap.min.js') }}
    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    {{ HTML::script('assets/backend/AdminLTE/plugins/morris/morris.min.js') }}
    <!-- Sparkline -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') }}
    <!-- jvectormap -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
    {{ HTML::script('assets/backend/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
    <!-- jQuery Knob Chart -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/knob/jquery.knob.js') }}
    <!-- daterangepicker -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/daterangepicker/daterangepicker.js') }}
    <!-- datepicker -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') }}
    <!-- Bootstrap WYSIHTML5 -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
    <!-- iCheck -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/iCheck/icheck.min.js') }}
    <!-- Slimscroll -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}
    <!-- FastClick -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/fastclick/fastclick.min.js') }}
    <!-- AdminLTE App -->
    {{ HTML::script('assets/backend/AdminLTE/dist/js/app.min.js') }}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- {{ HTML::script('assets/backend/AdminLTE/dist/js/pages/dashboard.js') }} -->
    <!-- AdminLTE for demo purposes -->
    <!-- {{ HTML::script('assets/backend/AdminLTE/dist/js/demo.js') }} -->

    <!-- DATA TABES SCRIPT -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/datatables/jquery.dataTables.js') }}
    {{ HTML::script('assets/backend/AdminLTE/plugins/datatables/dataTables.bootstrap.js') }}
    <!-- page script -->
    <script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable({
            "aaSorting": [[0,'desc']],
            "oLanguage": {
              "sUrl": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Indonesian.json"
            }
        });

        // Modal
        $('#exampleModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var recipient = button.data('whatever') // Extract info from data-* attributes
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this)
          modal.find('.form-action').attr('action', recipient)
          // $('#form-delete').attr('action' + recipient)
        });
    });
    </script>

     <!-- CK Editor -->
    {{ HTML::script('assets/backend/AdminLTE/plugins/ckeditor/ckeditor.js') }}
    <script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
      });
    </script>
  </body>
</html>

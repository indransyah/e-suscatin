@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  @include('backend.layouts.callout')
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $pages }}</h3>
          <p>Halaman</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-book-outline"></i>
        </div>
        <a href="{{ URL::action('AdminHalamanController@getIndex') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $lessons }}</h3>
          <p>Materi</p>
        </div>
        <div class="icon">
          <i class="ion ion-document-text"></i>
        </div>
        <a href="{{ URL::action('AdminMateriController@getIndex') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $users }}</h3>
          <p>Petugas</p>
        </div>
        <div class="icon">
          <i class="ion ion-person"></i>
        </div>
        <a href="{{ URL::action('AdminPetugasController@getIndex') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $couple }}</h3>
          <p>Pasangan Pengantin</p>
        </div>
        <div class="icon">
          <i class="ion ion-heart"></i>
        </div>
        <a href="{{ URL::action('AdminPengantinController@getIndex') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
  </div><!-- /.row -->
</section><!-- /.content -->

@stop
@extends('backend.layouts.master')
@section('content')
      <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Lihat Testimoni
            <small>melihat testimoni-testimoni yang dikirimkan oleh para pengantin.</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <!-- The time line -->
              <ul class="timeline">
                @foreach($testimonies as $testimoni)
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-green">
                    {{ Helpers::date($testimoni->created_at) }}
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-comments bg-green"></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header"><a href="{{ URL::action('AdminPengantinController@getDetail', $testimoni->user->couple->id) }}">{{ $testimoni->user->name }}</a> mengirimkan testimoni</h3>
                    <div class="timeline-body">
                      "{{ $testimoni->testimony }}"
                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                @endforeach
                <li>
                  <i class="fa fa-clock-o bg-gray"></i>
                </li>
              </ul>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
@stop

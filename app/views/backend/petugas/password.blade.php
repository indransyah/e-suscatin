@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Password Petugas
            <small>password akun Anda</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Password Petugas</h3>
                </div><!-- /.box-header -->
                <!-- form start --> 
                {{ Form::open(array('action' => 'AdminPetugasController@postPassword')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Password saat ini *</label> 
                      {{ Form::password('current_password', array('class' => 'form-control', 'placeholder' => 'Password yang Anda gunakan saat ini', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Password baru *</label> 
                      {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password baru yang akan digunakan', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Konfirmasi Password baru *</label>
                      {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Konfirmasi password baru (harus sama dengan Password baru)', 'required' => 'true' )) }}
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-info">Sunting <i class="fa fa-edit"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop
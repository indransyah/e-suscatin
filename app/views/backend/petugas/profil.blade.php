@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Profil Petugas
            <small>profil akun Anda</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Petugas</h3>
                </div><!-- /.box-header -->
                <!-- form start --> 
                {{ Form::open(array('action' => 'AdminPetugasController@postProfil')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama *</label> 
                      {{ Form::text('nama', Sentry::getUser()->name, array('class' => 'form-control', 'placeholder' => 'Nama lengkap petugas', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      {{ Form::text('email', Sentry::getUser()->email, array('class' => 'form-control', 'placeholder' => 'Alamat Email' )) }}
                    </div>
                    <div class="form-group">
                      <label>Username *</label>
                      {{ Form::text('username', Sentry::getUser()->username, array('class' => 'form-control', 'placeholder' => 'Username yang akan digunakan untuk masuk ke dalam halaman admin', 'required' => 'true' )) }}
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <a href="{{ URL::action('AdminPetugasController@getPassword') }}">Ingin merubah password?</a>
                    <button type="submit" class="pull-right btn btn-info">Sunting <i class="fa fa-edit"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop
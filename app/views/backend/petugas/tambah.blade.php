@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Petugas
            <small>menambah petugas yang dapat masuk ke dalam halaman admin</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Petugas</h3>
                </div><!-- /.box-header -->
                <!-- form start --> 
                {{ Form::open(array('action' => 'AdminPetugasController@postTambah')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama *</label> 
                      {{ Form::text('nama', null, array('class' => 'form-control', 'placeholder' => 'Nama lengkap petugas', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Alamat Email' )) }}
                    </div>
                    <div class="form-group">
                      <label>Username *</label>
                      {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username yang akan digunakan untuk masuk ke dalam halaman admin', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Password *</label> 
                      {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password yang akan digunakan untuk masuk ke dalam halaman admin', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Konfirmasi Password *</label>
                      {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Konfirmasi password (harus sama dengan Password)', 'required' => 'true' )) }}
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Tambah <i class="fa fa-plus"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop
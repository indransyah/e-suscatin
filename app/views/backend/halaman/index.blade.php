@extends('backend.layouts.master')
@section('content')
      <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Lihat Daftar Halaman
            <small>melihat daftar halaman yang terdapat pada e-learning</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar halaman</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="15%">Tanggal</th>
                        <th>Judul</th>
                        <th>Link</th>
                        <th width="15%">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($pages as $page)
                      <tr>
                        <td>{{ Helpers::date($page->created_at) }}</td>
                        <td>{{ $page->title }}</td>
                        <td>
                          @if($page->slug == 'beranda')
                          <a href="{{ URL::action('BerandaController@getIndex') }}">{{ URL::action('BerandaController@getIndex') }}</a>
                          @else
                          <a href="{{ URL::action('HalamanController@getLihat',$page->slug) }}">{{ URL::action('HalamanController@getLihat',$page->slug) }}</a>
                          @endif
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href="{{ URL::action('AdminHalamanController@getSunting', $page->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Sunting</a>
                            <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ URL::action('AdminHalamanController@deleteHapus', $page->id) }}">Hapus <i class="fa fa-trash"></i></a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <small><i>Tambahkan halaman dengan judul "Beranda" untuk mengganti halaman depan yang telah ada.</i></small>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h4>
              </div>
              <div class="modal-body">
                <h4>Data akan dihapus dari database dan tidak dapat dikembalikan lagi?</h4>
              </div>
              <div class="modal-footer">
                <form class="form-action" method="post">
                <input type="hidden" name="_method" value="DELETE">
                {{ Form::token() }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        
@stop

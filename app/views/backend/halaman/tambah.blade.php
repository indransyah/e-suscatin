@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Halaman
            <small>menambah halaman yang terdapat pada e-learning</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Halaman</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('action' => 'AdminHalamanController@postTambah')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul halaman *</label>
                      {{ Form::text('judul', null, array('class' => 'form-control', 'placeholder' => 'Judul halaman yang akan ditambahkan', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Isi halaman *</label>
                      {{ Form::textarea('isi', null, array('id' => 'editor1', 'class' => 'form-control', 'placeholder' => 'Isi halaman yang akan ditambahkan' )) }}
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Tambah <i class="fa fa-plus"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop
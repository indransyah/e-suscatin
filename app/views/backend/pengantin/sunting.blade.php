@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sunting Pengantin
            <small>mengubah data pasangan pengantin yang telah terdaftar pada e-learning</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')

          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Pengantin</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('action' => array('AdminPengantinController@postSunting', $id))) }}
                  <div class="box-body">
                    <div class="col-md-6">
                    <h4>Pengantin Pria</h4>
                    <div class="form-group">
                      <label>Nama *</label>
                      {{ Form::text('nama-pengantin-pria', $male->name, array('class' => 'form-control', 'placeholder' => 'Nama lengkap pengantin pria', 'required' => 'true')) }}
                    </div>
                    <div class="form-group">
                      <label>No Telp *</label>
                      {{ Form::text('telp-pengantin-pria', $male->phone, array('class' => 'form-control', 'placeholder' => 'No telp pengantin pria', 'required' => 'true')) }}
                    </div>
                    <div class="form-group">
                      <label>Agama</label>
                      {{ Form::select('agama-pengantin-pria', $agama, $male->religion, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                      <label>Tempat tanggal lahir</label>
                      {{ Form::text('ttl-pengantin-pria', $male->birth, array('class' => 'form-control', 'placeholder' => 'Tempat tanggal lahir pengantin pria')) }}
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      {{ Form::select('status-pengantin-pria', $status['pria'], $male->status, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      {{ Form::textarea('alamat-pengantin-pria', $male->address, array('class' => 'form-control', 'placeholder' => 'Alamat pengantin pria')) }}
                    </div>
                    </div>
                    <div class="col-md-6">
                    <h4>Mempelai wanita</h4>
                    <div class="form-group">
                      <label>Nama *</label>
                      {{ Form::text('nama-pengantin-wanita', $female->name, array('class' => 'form-control', 'placeholder' => 'Nama lengkap pengantin wanita', 'required' => 'true')) }}
                    </div>
                    <div class="form-group">
                      <label>No Telp *</label>
                      {{ Form::text('telp-pengantin-wanita', $female->phone, array('class' => 'form-control', 'placeholder' => 'No telp pengantin wanita', 'required' => 'true')) }}
                    </div>
                    <div class="form-group">
                      <label>Agama</label>
                      {{ Form::select('agama-pengantin-wanita', $agama, $female->religion, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                      <label>Tempat tanggal lahir</label>
                      {{ Form::text('ttl-pengantin-wanita', $female->birth, array('class' => 'form-control', 'placeholder' => 'Tempat tanggal lahir pengantin wanita')) }}
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      {{ Form::select('status-pengantin-wanita', $status['wanita'], $female->status, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      {{ Form::textarea('alamat-pengantin-wanita', $female->address, array('class' => 'form-control', 'placeholder' => 'Alamat pengantin wanita')) }}
                    </div>
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Sunting <i class="fa fa-edit"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop

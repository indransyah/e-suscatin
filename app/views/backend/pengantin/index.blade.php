@extends('backend.layouts.master')
@section('content')
      <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Lihat Daftar Pengantin
            <small>melihat daftar pengantin yang dapat mengakses halaman e-learning</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar pengantin</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="10%">Tanggal</th>
                        <th>Nama</th>
                        <th>Telp</th>
                        <th width="10%">Akses</th>
                        <th width="25%">Alamat</th>
                        <th width="21%">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($couple as $value)
                      <tr>
                        <td>{{ Helpers::date($value->created_at) }}</td>
                        <td>{{ $value->person[0]->name }} ({{ ucwords($value->person[0]->religion) }}) & {{ $value->person[1]->name }} ({{ ucwords($value->person[1]->religion) }})</td>
                        <td>{{ $value->person[0]->name }} : {{ $value->person[0]->phone }} <br> {{ $value->person[1]->name }} : {{ $value->person[1]->phone }}</td>
                        <td>{{ ($value->user->last_login == NULL) ? '-' : Helpers::date($value->user->last_login) }}</td>
                        <td>{{ $value->person[0]->name }} : {{ $value->person[0]->address }} <br> {{ $value->person[1]->name }} : {{ $value->person[1]->address }}</td>
                        <td>
                          <div class="btn-group">
                            <a href="{{ URL::action('AdminPengantinController@getDetail', $value->id) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> Detail</a>
                            <a href="{{ URL::action('AdminPengantinController@getSunting', $value->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Sunting</a>
                            <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ URL::action('AdminPengantinController@deleteHapus', $value->id) }}">Hapus <i class="fa fa-trash"></i></a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h4>
              </div>
              <div class="modal-body">
                <h4>Data akan dihapus dari database dan tidak dapat dikembalikan lagi?</h4>
              </div>
              <div class="modal-footer">
                <form class="form-action" method="post">
                <input type="hidden" name="_method" value="DELETE">
                {{ Form::token() }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
              </div>
            </div>
          </div>
        </div>

@stop

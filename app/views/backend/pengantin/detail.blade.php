@extends('backend.layouts.master')
@section('content')
      <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detail Pengantin
            <small>melihat detail pasangan pengantin</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <div class="col-md-6">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-male"></i>
                  <h3 class="box-title">Pengantin Pria</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <dl class="dl-horizontal">
                    <dt>Nama</dt>
                    <dd>{{ $couple->person[0]->name }}</dd>
                    <dt>Agama</dt>
                    <dd>{{ ucwords($couple->person[0]->religion) }}</dd>
                    <dt>Tempat tanggal lahir</dt>
                    <dd>{{ $couple->person[0]->birth }}</dd>
                    <dt>Telp</dt>
                    <dd>{{ $couple->person[0]->phone }}</dd>
                    <dt>Status</dt>
                    <dd>{{ ucwords($couple->person[0]->status) }}</dd>
                    <dt>Alamat</dt>
                    <dd>{{ $couple->person[0]->address }}</dd>
                  </dl>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <div class="col-md-6">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-female"></i>
                  <h3 class="box-title">Pengantin Wanita</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <dl class="dl-horizontal">
                    <dt>Nama</dt>
                    <dd>{{ $couple->person[1]->name }}</dd>
                    <dt>Agama</dt>
                    <dd>{{ ucwords($couple->person[1]->religion) }}</dd>
                    <dt>Tempat tanggal lahir</dt>
                    <dd>{{ $couple->person[1]->birth }}</dd>
                    <dt>Telp</dt>
                    <dd>{{ $couple->person[1]->phone }}</dd>
                    <dt>Status</dt>
                    <dd>{{ ucwords($couple->person[1]->status) }}</dd>
                    <dt>Alamat</dt>
                    <dd>{{ $couple->person[1]->address }}</dd>
                  </dl>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-comment-o"></i>
                  <h3 class="box-title">Testimoni</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  @if(is_null($couple->user->testimony))
                  <p><i>Pengantin belum mengirimkan testimoni</i></p>
                  @else
                  <p>"{{ $couple->user->testimony->first()->testimony }}"</p>
                  <p>- {{ $couple->person[0]->name }} & {{ $couple->person[1]->name }} -</p>
                  @endif
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop

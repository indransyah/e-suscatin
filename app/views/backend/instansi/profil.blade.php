@extends('backend.layouts.master')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Profil Instansi
            <small>detail instansi</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          @include('backend.layouts.callout')
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Detail Instansi</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('action' => 'AdminInstansiController@postProfil')) }}
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama Instansi *</label>
                      {{ Form::text('nama', $institute->name, array('class' => 'form-control', 'placeholder' => 'Nama instansi', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Alamat *</label>
                      {{ Form::textarea('alamat', $institute->address, array('class' => 'form-control', 'placeholder' => 'Alamat instansi', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>No Telp *</label>
                      {{ Form::text('telp', $institute->phone, array('class' => 'form-control', 'placeholder' => 'No telp', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                      <label>Nama Website *</label>
                      {{ Form::text('nama-website', $web->name, array('class' => 'form-control', 'placeholder' => 'Nama website', 'required' => 'true' )) }}
                      <small><i>Nama website akan muncul pada header (atas) halaman utama e-learning.</i></small>
                    </div>
                    <div class="form-group">
                      <label>Descripsi Website *</label>
                      {{ Form::textarea('deskripsi-website', $web->description, array('class' => 'form-control', 'placeholder' => 'Deskripsi tentang website', 'required' => 'true' )) }}
                      <small><i>Deskripsi website akan muncul di bawah nama website halaman utama e-learning.</i></small>
                    </div>
                    <small><i>* harus diisi</i></small>
                  </div><!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-success">Simpan <i class="fa fa-save"></i></button>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
@stop

<?php

class BerandaController extends \BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex()
	{
		$institute = Institute::find(1);
		$page = Page::whereSlug('beranda')->first();
		if (is_null($page)) {
			$data['title'] = 'Selamat Datang!';
			$data['content'] = 'Selamat datang di '.$institute->app_name.' '.$institute->name;
		} else {
			$data['title'] = $page->title;
			$data['content'] = $page->content;
		}
		$this->layout->content = View::make('frontend.beranda', compact('data'));
	}

	public function postMasuk()
	{
		try
		{
    		// Login credentials
			$credentials = array(
				'username'    => Input::get('username'),
				'password' => Input::get('password'),
				);
    		// Authenticate the user
			$user = Sentry::authenticate($credentials, false);
			if(!$user->hasAccess('pengantin')) {
				Sentry::logout();
				return Redirect::to('/')->withDanger('Anda tidak memiliki wewenang untuk masuk ke dalam sistem!');
			}
			return Redirect::intended(URL::action('MateriController@getIndex'))
                ->with('success', 'Selamat datang <strong>'.Sentry::getUser()->name.'</strong>, Anda berhasil login!');
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::to('/')
                ->with('danger', 'Form username tidak boleh kosong!')->withInput();;
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::to('/')
                ->with('danger', 'Form password tidak boleh kosong!')->withInput();;
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::to('/')
                ->with('danger', 'Password Anda salah!')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::to('/')
                ->with('danger', 'User tidak ditemukan')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::to('/')
                ->with('danger', 'User tidak aktif')->withInput();;
        }

	}

	public function getKeluar()
	{
		Sentry::logout();
		return Redirect::action('BerandaController@getIndex')
            ->with('warning', 'Anda telah berhasil keluar!');
	}

}

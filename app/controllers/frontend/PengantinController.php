<?php

class PengantinController extends \BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex()
	{
		return Redirect::action('BerandaController@getIndex');
	}

	public function getPassword()
	{
		$this->layout->content = View::make('frontend.pengantin.password');
	}

	public function postPassword()
	{
		$user = Sentry::findUserById(Sentry::getUser()->id);
    	
    	$check = $user->checkPassword(Input::get('current_password'));
    	if (!$check) {
    		return Redirect::action('PengantinController@getPassword')
                ->with('danger', 'Password saat ini salah!');
    	} 

    	$rules = User::$rules; 
    	unset($rules['nama']); 
    	unset($rules['username']); 
    	unset($rules['email']);
    	// return $rules;
    	$validator = Validator::make(Input::all(), $rules); 
    	if ($validator->fails()) {
    		return Redirect::action('PengantinController@getPassword') 
    			->with('danger', 'Data yang Anda isikan salah!') 
    			->withErrors($validator) 
    			->withInput(); 
    	}

    	$user->password = Input::get('password'); 
    	$user->save(); 
    	return Redirect::action('PengantinController@getPassword') 
    		->with('success', 'Password berhasil diubah!');
	}

}
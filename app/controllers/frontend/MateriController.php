<?php

class MateriController extends \BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex()
	{
		$lesson = Lesson::all();
		$this->layout->content = View::make('frontend.materi.index', compact('lesson'));
	}

	public function getLihat($id)
	{
		$lesson = Lesson::find($id);
		$this->layout->content = View::make('frontend.materi.lihat', compact('lesson'));
	}

	public function getCari()
	{
		$keyword = Input::get('q');
		$lesson = Lesson::where('title', 'like', '%'.$keyword.'%')->get();
		$this->layout->content = View::make('frontend.materi.cari', compact('lesson', 'keyword'));
	}

}

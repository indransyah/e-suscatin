<?php

class TestimoniController extends \BaseController {

	protected $layout = 'frontend.layouts.master';

	public function postKirim()
	{
		// return Session::get('testimony');
		// Session::put('testimony', Request::path());
		$validator = Validator::make(Input::all(), Testimony::$rules);
		if ($validator->fails())
		{
			return Redirect::to(Input::get('redirect'))
				->with('danger', 'Data yang Anda isikan salah!')
				->withErrors($validator)
				->withInput();
		}
		$testimony = Testimony::create(array(
			'testimony' => Input::get('testimoni'),
			'user_id'		=> Sentry::getUser()->id
		));
		return Redirect::to(Input::get('redirect'))
			->withSuccess('Testimoni telah berhasil ditambahkan');
	}

}

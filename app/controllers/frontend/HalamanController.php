<?php

class HalamanController extends \BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getLihat($slug)
	{
		// return Request::path(); // halaman/lihat/panduan
		// return Request::method(); // GET
		// return Request::url(); // http://localhost:8000/halaman/lihat/panduan
		// return Request::segment(1); // halaman

		$page = Page::whereSlug($slug)->first();
		$this->layout->content = View::make('frontend.halaman.lihat', compact('page'));
	}

}
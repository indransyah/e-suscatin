<?php

class AdminTestimoniController extends \BaseController {

  protected $layout = 'backend.layouts.master';

  public function getIndex() {
    $testimonies = Testimony::with('user.couple.person')->get();
    $this->layout->content = View::make('backend.testimoni.index', compact('testimonies'));
  }
}

<?php

class AdminInstansiController extends \BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex()
	{
		$this->getProfil();
	}

	public function getProfil()
	{
		$institute = Institute::find(1);
		$web = Web::find(1);
		$this->layout->content = View::make('backend.instansi.profil', compact('institute', 'web'));
	}

	public function postProfil()
	{
		$validator = Validator::make(Input::all(), Institute::$rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminInstansiController@getProfil')
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }
        $instansi = Institute::find(1);
        $instansi->name = Input::get('nama');
        $instansi->phone = Input::get('telp');
        $instansi->address = Input::get('alamat');
        $instansi->app_name = Input::get('nama-website');
        $instansi->save();
        return Redirect::action('AdminInstansiController@getProfil')
        	->withSuccess('Profil instansi telah berhasil diperbarui.');
	}

}

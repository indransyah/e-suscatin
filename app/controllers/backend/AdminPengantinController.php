<?php

class AdminPengantinController extends \BaseController {

	protected $layout = 'backend.layouts.master';

	protected $rules = array(
		'nama-pengantin-pria' 	=> 'required',
		'nama-pengantin-wanita' => 'required',
		'telp-pengantin-pria' 	=> 'required|digits_between:11,12|unique:person,phone',
		'telp-pengantin-wanita' => 'required|digits_between:11,12|unique:person,phone',
		// 'agama-pengantin-pria' 	=> 'required',
		// 'agama-pengantin-wanita' => 'required',
		'ttl-pengantin-pria' 	=> 'required',
		'ttl-pengantin-wanita' => 'required',
	);

	protected $agama = array(
		'islam'		=> 'Islam',
		'kristen'	=> 'Kristen',
		'katolik'	=> 'Katolik',
		'hindu'		=> 'Hindu',
		'budha'		=> 'Budha',
		'konghucu'		=> 'Konghucu',
	);

	protected $status = array(
		'pria'		=> array(
			'jejaka'			=> 'Jejaka',
			'duda cerai'	=> 'Duda Cerai',
			'duda mati'		=> 'Duda Mati'
			),
		'wanita'	=> array(
			'perawan'			=> 'Perawan',
			'janda cerai'	=> 'Janda Cerai',
			'janda mati'	=> 'Janda Mati')
	);

	public function getIndex()
	{
		$couple = Couple::with('person')->get();
		$this->layout->content = View::make('backend.pengantin.index', compact('couple'));
	}

	public function getTambah()
	{
		$status = $this->status;
		$agama = $this->agama;
		$this->layout->content = View::make('backend.pengantin.tambah', compact('status', 'agama'));
	}

	public function postTambah()
	{
		$input = Input::except('_token');
		if ($input['telp-pengantin-pria'] == $input['telp-pengantin-wanita']) {
			return Redirect::action('AdminPengantinController@getTambah')
            	->withDanger('No telp tidak boleh sama')
            	->withInput();
		}
		$validator = Validator::make($input, $this->rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminPengantinController@getTambah')
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }

		Event::fire('create.user', array($input));

		return Redirect::action('AdminPengantinController@getIndex')
			->withSuccess('Pasangan pengantin telah berhasil ditambahkan.');
	}

	public function getDetail($id)
	{
		$couple = Couple::whereId($id)->with(array('person', 'user.testimony'))->first();
		$this->layout->content = View::make('backend.pengantin.detail', compact('couple'));
	}

	public function getSunting($id) {
		$status = $this->status;
		$agama = $this->agama;
		$couple = Couple::find($id);
		$male = $couple->person[0];
		$female = $couple->person[1];
		$this->layout->content = View::make('backend.pengantin.sunting', compact('id', 'male', 'female', 'status', 'agama'));
	}

	public function postSunting($id) {
		$input = Input::except('_token');
		$couple = Couple::find($id);
		$rules = $this->rules;
		$rules['telp-pengantin-pria'] .= ',' . $couple->person[0]->id . ',id';
		$rules['telp-pengantin-wanita'] .= ',' . $couple->person[1]->id . ',id';
		$validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminPengantinController@getSunting', $id)
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }
		Event::fire('update.person', array($id, $input));
		return Redirect::action('AdminPengantinController@getSunting', $id)
			->withSuccess('Data pengantin telah berhasil diubah.');

	}

	public function deleteHapus($id)
	{
		$couple = Couple::find($id);
		$couple->delete();
		return Redirect::action('AdminPengantinController@getIndex')
			->withSuccess('Pengantin telah berhasil dihapus');
	}



}

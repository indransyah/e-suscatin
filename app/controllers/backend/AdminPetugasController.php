<?php

class AdminPetugasController extends \BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex()
	{
		// $users = User::all();
		$group = Sentry::findGroupByName('Administrator');
		$users = Sentry::findAllUsersInGroup($group);
		$this->layout->content = View::make('backend.petugas.index', compact('users'));
	}

	public function getTambah()
	{
		$this->layout->content = View::make('backend.petugas.tambah');
	}

	public function postTambah()
	{
		$validator = Validator::make(Input::all(), User::$rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminPetugasController@getTambah')
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }
        // Create the user
        $user = Sentry::createUser(array(
        	'name'      => Input::get('nama'),
        	'username'  => Input::get('username'),
        	'email'     => Input::get('email'),
        	'password'  => Input::get('password'),
        	'activated' => true,
        	));
        // Find the group using the group id
        $administratorGroup = Sentry::findGroupByName('Administrator');
        // Assign the group to the user
        $user->addGroup($administratorGroup);
        return Redirect::action('AdminPetugasController@getIndex')
        	->with('success', 'Petugas telah berhasil ditambahkan.');
	}

	public function getProfil()
	{
		$this->layout->content = View::make('backend.petugas.profil');
	}

	public function postProfil()
	{
		$rules = User::$rules;
		$rules['username'] .= ',' . Sentry::getUser()->id . ',id';
		$rules['email'] .= ',' . Sentry::getUser()->id . ',id';
    	unset($rules['password']);
    	unset($rules['password_confirmation']);
		$validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminPetugasController@getProfil')
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }
        $user = Sentry::findUserById(Sentry::getUser()->id);
    	$user->name = Input::get('nama');
    	$user->username = Input::get('username');
    	$user->email = Input::get('email');
    	$user->save();
    	return Redirect::action('AdminPetugasController@getProfil')
    	  ->with('success', 'Profil berhasil diubah.');
	}

	public function deleteHapus($id)
	{
		try
		{
    		// Find the user using the user id
			$user = Sentry::findUserById($id);
    		// Delete the user
			$user->delete();
			return Redirect::action('AdminPetugasController@getIndex')
				->with('success', 'Petugas telah berhasil dihapus');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::action('AdminPetugasController@getIndex')
				->with('danger', 'Petugas tidak ditemukan');
		}
	}

	public function getPassword()
	{
		$this->layout->content = View::make('backend.petugas.password');
	}

	public function postPassword()
	{
		$user = Sentry::findUserById(Sentry::getUser()->id);

    	$check = $user->checkPassword(Input::get('current_password'));
    	if (!$check) {
    		return Redirect::action('AdminPetugasController@getPassword')
                ->with('danger', 'Password saat ini salah!');
    	}

    	$rules = User::$rules;
    	unset($rules['nama']);
    	unset($rules['username']);
    	unset($rules['email']);
    	$validator = Validator::make(Input::all(), $rules);
    	if ($validator->fails()) {
    		return Redirect::action('AdminPetugasController@getPassword')
    			->with('danger', 'Data yang Anda isikan salah!')
    			->withErrors($validator)
    			->withInput();
    	}

    	$user->password = Input::get('password');
    	$user->save();
    	return Redirect::action('AdminPetugasController@getPassword')
    		->with('success', 'Password berhasil diubah!');
	}

}

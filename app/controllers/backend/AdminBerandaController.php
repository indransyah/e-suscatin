<?php

class AdminBerandaController extends \BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex()
	{
		$couple = Couple::all()->count();
		// $users = Sentry::findAllUsersWithAccess('admin');
		$group = Sentry::findGroupByName('Administrator');
		$users = Sentry::findAllUsersInGroup($group)->count();
		// return $users;
		$lessons = Lesson::all()->count();
		$pages = Page::all()->count();
		$this->layout->content = View::make('backend.dashboard', compact('couple', 'users', 'lessons', 'pages'));
	}

	public function getMasuk()
	{
		return View::make('backend.login');
	}

	public function postMasuk()
	{
		try
		{
    		// Login credentials
			$credentials = array(
				'username'    => Input::get('username'),
				'password' => Input::get('password'),
				);
    		// Authenticate the user
			$user = Sentry::authenticate($credentials, false);
			if(!$user->hasAccess('admin')) {
				Sentry::logout();
				return Redirect::action('AdminBerandaController@getMasuk')->withDanger('Anda tidak memiliki wewenang untuk masuk ke dalam sistem!');
			}
			return Redirect::intended(URL::action('AdminBerandaController@getIndex'))
                ->with('success', 'Selamat datang '.Sentry::getUser()->name.', Anda berhasil masuk!');
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::action('AdminBerandaController@getMasuk')
                ->with('danger', 'Form username tidak boleh kosong!')->withInput();;
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::action('AdminBerandaController@getMasuk')
                ->with('danger', 'Form password tidak boleh kosong!')->withInput();;
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::action('AdminBerandaController@getMasuk')
                ->with('danger', 'Password Anda salah!')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::action('AdminBerandaController@getMasuk')
                ->with('danger', 'Petugas tidak ditemukan')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::action('AdminBerandaController@getMasuk')
                ->with('danger', 'Petugas tidak aktif')->withInput();;
        }
	}

	public function getKeluar()
	{
		Sentry::logout();
		return Redirect::action('AdminBerandaController@getMasuk')
            ->with('success', 'Anda telah berhasil keluar!');
	}



}

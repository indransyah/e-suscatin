<?php

class AdminHalamanController extends \BaseController {

    protected $layout = 'backend.layouts.master';

    public function getIndex()
    {
        $pages = Page::all();
        $this->layout->content = View::make('backend.halaman.index', compact('pages'));
    }

    public function getTambah()
    {
        $this->layout->content = View::make('backend.halaman.tambah');
    }

    public function postTambah()
    {
        $validator = Validator::make(Input::all(), Page::$rules);
        if ($validator->fails())
        {
            return Redirect::action('AdminHalamanController@getTambah')
                ->with('danger', 'Data yang Anda isikan salah!')
                ->withErrors($validator)
                ->withInput();
        }
        $page = Page::create(array(
                'title' => Input::get('judul'),
                'content' => Input::get('isi'),
                'slug' => Str::slug(Input::get('judul'), '-')
            ));
        return Redirect::action('AdminHalamanController@getIndex')
            ->withSuccess('Halaman telah berhasil ditambahkan');
    }

    public function getSunting($id)
    {
        $page = Page::find($id);
        $this->layout->content = View::make('backend.halaman.sunting', compact('page'));
    }

    public function postSunting($id)
    {
        $rules = Page::$rules;
        $rules['judul'] .= ',' . $id . ',id';
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::action('AdminHalamanController@getSunting', $id)
                ->with('danger', 'Data yang Anda isikan salah!')
                ->withErrors($validator)
                ->withInput();
        }
        $page = Page::find($id);
        $page->title = Input::get('judul');
        $page->content = Input::get('isi');
        $page->slug = Str::slug(Input::get('judul'), '-');
        $page->save();
        return Redirect::action('AdminHalamanController@getIndex')
            ->withSuccess('Halaman telah berhasil disunting.');
    }

    public function deleteHapus($id)
    {
        $page = Page::find($id);
        $page->delete();
        return Redirect::action('AdminHalamanController@getIndex')
            ->withSuccess('Halaman telah berhasil dihapus');
    }


}
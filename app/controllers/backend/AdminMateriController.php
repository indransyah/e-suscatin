<?php

class AdminMateriController extends \BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex()
	{
		$lessons = Lesson::all();
		$this->layout->content = View::make('backend.materi.index', compact('lessons'));
	}

	public function getTambah()
	{
		$this->layout->content = View::make('backend.materi.tambah');
	}

	public function postTambah()
	{
		$validator = Validator::make(Input::all(), Lesson::$rules);
		if ($validator->fails())
		{
			return Redirect::action('AdminMateriController@getTambah')
			->with('danger', 'Data yang Anda isikan salah!')
			->withErrors($validator)
			->withInput();
		}
		$materi = Lesson::create(array(
			'title' => Input::get('judul'),
			'description' => Input::get('deskripsi'),
			'file' => Input::file('file')
		));
		return Redirect::action('AdminMateriController@getIndex')
		->withSuccess('Materi telah berhasil ditambahkan');
	}

	public function getSunting($id)
	{
		$lesson = Lesson::find($id);
		$this->layout->content = View::make('backend.materi.sunting', compact('lesson'));
	}

	public function postSunting($id)
	{
		$rules = Lesson::$rules;
    	unset($rules['file']);
    	$rules = array_add($rules, 'file', 'mimes:doc,docx,pdf,ppt,pptx');
		$validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
        	return Redirect::action('AdminMateriController@getSunting', $id)
            	->with('danger', 'Data yang Anda isikan salah!')
            	->withErrors($validator)
            	->withInput();
        }
        $materi = Lesson::find($id);
        $materi->title = Input::get('judul');
        $materi->description = Input::get('deskripsi');
        $materi->file = Input::file('file');
        $materi->save();
        return Redirect::action('AdminMateriController@getIndex')
        	->withSuccess('Materi telah berhasil disunting.');
	}

	public function deleteHapus($id)
	{
		$lesson = Lesson::find($id);
		$lesson->delete();
		return Redirect::action('AdminMateriController@getIndex')
			->withSuccess('Materi telah berhasil dihapus');
	}


}
